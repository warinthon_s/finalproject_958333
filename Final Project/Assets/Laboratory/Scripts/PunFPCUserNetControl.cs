using UnityEngine;
using StarterAssets;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;


[RequireComponent(typeof(PhotonView))]
public class PunFPCUserNetControl : MonoBehaviourPunCallbacks, IPunInstantiateMagicCallback {
    [Tooltip("The local player instance. Use this to know if the local player is represented in the Scene")]
    public static GameObject LocalPlayerInstance;
    public Transform CameraRoot;

    public StarterAssetsInputs _input;

    public void OnPhotonInstantiate(PhotonMessageInfo info) {
        Debug.Log(info.photonView.Owner.ToString());
        Debug.Log(info.photonView.ViewID.ToString());

        // #Important
        // used in PunNetworkManager.cs
        // : we keep track of the localPlayer instance to prevent instanciation when levels are synchronized
        if (photonView.IsMine) {
            LocalPlayerInstance = gameObject;
            GetComponentInChildren<MeshRenderer>().material.color = Color.blue;

            PunNetManagerVCam PunNMVCam = (PunNetManagerVCam)PunNetworkManager.singleton;
            if (PunNMVCam != null)
                PunNMVCam._vCam.Follow = CameraRoot;
            else Debug.LogWarning("PunNetManagerVCam is NULL.");

            //Call CustomProperties to other object.
            ChangeColorProperties();

            //Setup Local UI
            GetComponentInChildren<UIPlayerInfoManager>().SetLocalUI();
        }
        else {
            GetComponent<FirstPersonController>().enabled = false;
        }

        //Setup NickName
        GetComponentInChildren<UIPlayerInfoManager>().SetNickName(info.Sender.NickName);
    }

    void Update()
    {

        if (!photonView.IsMine)
            return;

        //Lab CAS
#if ENABLE_INPUT_SYSTEM
        if (_input != null)
        {
            if (_input.change)
            {
                _input.change = false;
                ChangeColorProperties();
            }
        }
        else
        {
            Debug.Log("StarterAssetsInputs is NULL.");
        }
#else
            if (Input.GetKeyDown(KeyCode.C))
                ChangeColorProperties();
#endif
        /////////
    }
    //Lab CAS
    private void ChangeColorProperties()
    {
        Hashtable props = new Hashtable
        {
            {PunGameSetting.PLAYER_COLOR, Random.Range(0,7)}
        };
        PhotonNetwork.LocalPlayer.SetCustomProperties(props);
    }

    public override void OnPlayerPropertiesUpdate(Player target, Hashtable changedProps)
    {
        base.OnPlayerPropertiesUpdate(target, changedProps);
        if (changedProps.ContainsKey(PunGameSetting.PLAYER_COLOR) &&
            target.ActorNumber == photonView.ControllerActorNr)
        {
            object colors;
            if (changedProps.TryGetValue(PunGameSetting.PLAYER_COLOR, out colors))
            {
                GetComponentInChildren<MeshRenderer>().material.color = PunGameSetting.GetColor((int)colors);
            }

            return;
        }
    }
    /////////
}
