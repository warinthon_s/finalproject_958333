﻿using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;
using Photon.Pun;
//Lab CAS
using Hashtable = ExitGames.Client.Photon.Hashtable;
/////////
using Photon.Realtime;

[RequireComponent(typeof(PhotonView))]
public class PunUserNetControl : MonoBehaviourPunCallbacks , IPunInstantiateMagicCallback
{
    [Tooltip("The local player instance. Use this to know if the local player is represented in the Scene")]
    public static GameObject LocalPlayerInstance;

    #region Photon Callback

    public void OnPhotonInstantiate(PhotonMessageInfo info)
    {
        Debug.Log(info.photonView.Owner.ToString());
        Debug.Log(info.photonView.ViewID.ToString());
        //PhotonNetwork.LocalPlayer.
        // #Important
        // used in PunNetworkManager.cs
        // : we keep track of the localPlayer instance to prevent instanciation when levels are synchronized
        if (photonView.IsMine) {
            LocalPlayerInstance = gameObject;

            //Call CustomProperties to other object.
            ChangeColorProperties();
            
        }
        else {
            GetComponentInChildren<Camera>().enabled = false;
            GetComponentInChildren<AudioListener>().enabled = false;
            GetComponent<FirstPersonController>().enabled = false;

            //Apply CustomProperties to other object.
            OnPlayerPropertiesUpdate(photonView.Owner, photonView.Owner.CustomProperties);
        }

        //Fix setting for Game Network State Management
        // #Critical
        // we flag as don't destroy on load so that instance survives level synchronization, thus giving a seamless experience when levels load.
        DontDestroyOnLoad(gameObject);
    }

    #endregion

    void Update()
    {

        if (!photonView.IsMine)
            return;

        //Lab CAS
        if (Input.GetKeyDown(KeyCode.C)) {
            ChangeColorProperties();
        }
        /////////
    }
    //Lab CAS
    private void ChangeColorProperties()
    {
        Hashtable props = new Hashtable
        {
            {PunGameSetting.PLAYER_COLOR, Random.Range(0,7)}
        };
        PhotonNetwork.LocalPlayer.SetCustomProperties(props);
    }

    public override void OnPlayerPropertiesUpdate(Player target, Hashtable changedProps)
    {
        base.OnPlayerPropertiesUpdate(target, changedProps);
        if (changedProps.ContainsKey(PunGameSetting.PLAYER_COLOR) &&
            target.ActorNumber == photonView.ControllerActorNr)
        {
            object colors;
            if (changedProps.TryGetValue(PunGameSetting.PLAYER_COLOR, out colors))
            {
                GetComponent<MeshRenderer>().material.color = PunGameSetting.GetColor((int)colors);
            }

            return;
        }
    }
    /////////

}
