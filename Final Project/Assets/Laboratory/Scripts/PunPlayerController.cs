﻿using UnityEngine;
using Photon.Pun;
using StarterAssets;

public class PunPlayerController : MonoBehaviourPun {
    public GameObject bulletPrefab;
    public Camera m_playerCam;
    public Transform FirePosition;
    public float BulletForce = 20f;
    
    private StarterAssetsInputs _input;
    void Start() {
        if (m_playerCam == null)
            m_playerCam = Camera.main;

        if (FirePosition == null)
            FirePosition = this.transform;

        _input = GetComponent<StarterAssetsInputs>();
    }

    void Update() {
        if (!photonView.IsMine)
            return;

#if ENABLE_INPUT_SYSTEM
        if (_input != null) {
            if (_input.fire) {
                _input.fire = false;
                CmdFire(m_playerCam.transform.rotation);
            }
        }else {
            Debug.Log("StarterAssetsInputs is NULL.");
        }
#else
            if (Input.GetKeyDown(KeyCode.Mouse0))
                CmdFire(m_playerCam.transform.rotation);
#endif

    }

    void CmdFire(Quaternion rotation) {
        //Keep Data when Instantiate.
        object[] data = { photonView.ViewID };

        // Spawn the bullet on the Clients and Create the Bullet from the Bullet Prefab
        Rigidbody bullet = PhotonNetwork.Instantiate(this.bulletPrefab.name
                                                , FirePosition.transform.position + (FirePosition.transform.forward * 1.5f)
                                                , rotation
                                                , 0
                                                , data).GetComponent<Rigidbody>();

        // Add velocity to the bullet
        bullet.velocity = bullet.transform.forward * BulletForce;

        // Destroy the bullet after 10 seconds
        Destroy(bullet.gameObject, 10.0f);
    }
}
